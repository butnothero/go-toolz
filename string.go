package gt

import "unicode"

// FirstLetterToLower перевести первый символ строки в нижний регистр.
func FirstLetterToLower(s string) string {
	if s == "" {
		return s
	}

	r := []rune(s)
	r[0] = unicode.ToLower(r[0])

	return string(r)
}

// FirstLetterToUpper перевести первый символ строки в верхний регистр.
func FirstLetterToUpper(s string) string {
	if s == "" {
		return s
	}

	r := []rune(s)
	r[0] = unicode.ToUpper(r[0])

	return string(r)
}
