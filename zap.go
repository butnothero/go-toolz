package gt

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type IZap interface {
	Desugar() *zap.Logger
	Named(name string) *zap.SugaredLogger
	WithOptions(opts ...zap.Option) *zap.SugaredLogger
	With(args ...any) *zap.SugaredLogger
	Level() zapcore.Level
	Debug(args ...any)
	Info(args ...any)
	Warn(args ...any)
	Error(args ...any)
	DPanic(args ...any)
	Panic(args ...any)
	Fatal(args ...any)
	Debugf(template string, args ...any)
	Infof(template string, args ...any)
	Warnf(template string, args ...any)
	Errorf(template string, args ...any)
	DPanicf(template string, args ...any)
	Panicf(template string, args ...any)
	Fatalf(template string, args ...any)
	Debugw(msg string, keysAndValues ...any)
	Infow(msg string, keysAndValues ...any)
	Warnw(msg string, keysAndValues ...any)
	Errorw(msg string, keysAndValues ...any)
	DPanicw(msg string, keysAndValues ...any)
	Panicw(msg string, keysAndValues ...any)
	Fatalw(msg string, keysAndValues ...any)
	Debugln(args ...any)
	Infoln(args ...any)
	Warnln(args ...any)
	Errorln(args ...any)
	DPanicln(args ...any)
	Panicln(args ...any)
	Fatalln(args ...any)
	Sync() error
}
