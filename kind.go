package gt

import "reflect"

func Ptr[T any](value T) *T {
	return &value
}

func ZeroVal[T any]() T {
	var zero T
	return zero
}

func IsBasicKind(value any) bool {
	kind := reflect.ValueOf(value).Kind()
	return ReflectIsBasicKind(kind)
}

func ReflectIsBasicKind(k reflect.Kind) bool {
	//nolint:exhaustive // ignore
	switch k {
	case
		reflect.Bool,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Uintptr,
		reflect.Float32,
		reflect.Float64,
		reflect.Complex64,
		reflect.Complex128,
		reflect.String,
		reflect.UnsafePointer:
		return true
	default:
		return false
	}
}
