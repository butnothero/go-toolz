package gt

import (
	"context"

	"github.com/jackc/pgx/v5"
)

type PgxGetterOption func(g *PgxGetter)

func PgxGetterWithPool(pool PgxPool) PgxGetterOption {
	return func(g *PgxGetter) {
		g.pool = pool
	}
}

type PgxGetter struct {
	pool     PgxPool
	ctxTxKey CtxKeyPgxTx
}

func NewPgxGetter(opts ...PgxGetterOption) *PgxGetter {
	g := &PgxGetter{
		ctxTxKey: defaultCtxKeyPgxTx,
	}

	for _, opt := range opts {
		opt(g)
	}

	return g
}

func (g *PgxGetter) MustTx(ctx context.Context) PgxTx {
	if tx, ok := ctx.Value(g.ctxTxKey).(pgx.Tx); ok {
		return tx
	}

	if g.pool == nil {
		panic("pgx getter must tx: g.pool is nil")
	}

	return g.pool
}

func (g *PgxGetter) Tx(ctx context.Context, pool PgxPool) PgxTx {
	if tx, ok := ctx.Value(g.ctxTxKey).(pgx.Tx); ok {
		return tx
	}

	if pool == nil {
		panic("pgx getter tx: pool is nil")
	}

	return pool
}
