package gt

import (
	"github.com/go-playground/validator/v10"
)

var (
	_validator *validator.Validate
)

func Validate() *validator.Validate {
	if _validator == nil {
		_validator = validator.New(validator.WithRequiredStructEnabled())
	}

	return _validator
}

type (
	ISimpleValidator interface {
		Validate(i any) error
	}
)

func PtrStringIsEmpty(s *string) bool {
	if s == nil {
		return false
	}

	if *s == "" {
		return true
	}

	return false
}
