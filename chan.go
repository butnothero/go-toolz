package gt

import (
	"reflect"
	"unsafe"
)

// IsChan Проверка, является ли переданный аргумент каналом.
func IsChan(ch any) bool {
	return reflect.TypeOf(ch).Kind() == reflect.Chan
}

// ChanIsClosed Проверить, закрыт канал или нет.
func ChanIsClosed(ch any) bool {
	if !IsChan(ch) {
		panic("is not a channel")
	}

	cptr := *(*uintptr)(unsafe.Pointer(uintptr(unsafe.Pointer(&ch)) + unsafe.Sizeof(uint(0))))

	var n uintptr = 2
	cptr += unsafe.Sizeof(uint(0)) * n
	cptr += unsafe.Sizeof(unsafe.Pointer(uintptr(0)))
	cptr += unsafe.Sizeof(uint16(0))

	return *(*uint32)(unsafe.Pointer(cptr)) > 0
}
