package gt_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestCallerPath(t *testing.T) {
	callerPath := gt.CallerPath()
	nameSlice := strings.Split(callerPath, "/")
	require.Equalf(
		t,
		"path_test.go",
		nameSlice[len(nameSlice)-1],
		"In test %s invalid file name",
		"Ok Filename",
	)
}

func TestReplaceIdxInPath(t *testing.T) {
	testCases := map[string]struct {
		path   string
		newEnd string
		idx    int
		check  func(result string)
	}{
		"OK": {
			path:   gt.CallerPath(),
			newEnd: "/path.go",
			idx:    1,
			check: func(result string) {
				resultSlice := strings.Split(result, "/")

				require.Equalf(
					t,
					"path.go",
					resultSlice[len(resultSlice)-1],
					"In test %s invalid file name replacement",
					"OK",
				)
			},
		},
		"OK empty path": {
			path:   "",
			newEnd: "/path.go",
			idx:    1,
			check: func(result string) {
				require.Equalf(
					t,
					"/path.go",
					result,
					"In test %s invalid file name replacement",
					"OK empty path",
				)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(i, func(t *testing.T) {
			newCallerFilename := gt.ReplaceIdxInPath(tc.path, tc.newEnd, tc.idx)
			tc.check(newCallerFilename)
		})
	}
}
