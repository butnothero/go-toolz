package gt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"io"

	"golang.org/x/crypto/bcrypt"
)

func EncryptPassword(password string, cost ...int) (string, error) {
	defaultCost := 14
	_cost := OptionalArg(cost, defaultCost)
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), _cost)

	return string(bytes), err
}

func VerifyEncryptedPassword(password, passwordHash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(passwordHash), []byte(password))
	return err == nil
}

// EncryptAES256 Шифрует заданную строку с помощью AES-256-CBC.
// Для использования нужен 256-битный ключ. Для генерации ключа
// можно воспользоваться сервисом https://save-editor.com/crypto/crypt_key_generator.html.
// Пример ключа: 54EC75D97320CB5B903E1D6AE322DB7A88FC304860F812A0BC61A79FC0E39094
func EncryptAES256(key, stringToEncrypt string) (string, error) {
	keyByte, err := hex.DecodeString(key)
	if err != nil {
		return "", err
	}

	plaintext := []byte(stringToEncrypt)

	var block cipher.Block

	block, err = aes.NewCipher(keyByte)
	if err != nil {
		return "", err
	}

	// IV должен быть уникальным, но не защищенным.
	// Поэтому обычно его включают в начало зашифрованного текста.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return base64.URLEncoding.EncodeToString(ciphertext), nil
}

// DecryptAES256 Расшифровать AES-256-CBC из base64 в строку.
// Для использования нужен 256-битный ключ. Для генерации ключа
// можно воспользоваться сервисом https://save-editor.com/crypto/crypt_key_generator.html.
// Пример ключа: 54EC75D97320CB5B903E1D6AE322DB7A88FC304860F812A0BC61A79FC0E39094
func DecryptAES256(key, stringToDecrypt string) (string, error) {
	keyByte, err := hex.DecodeString(key)
	if err != nil {
		return "", err
	}

	var ciphertext []byte

	ciphertext, err = base64.URLEncoding.DecodeString(stringToDecrypt)
	if err != nil {
		return "", err
	}

	var block cipher.Block

	block, err = aes.NewCipher(keyByte)
	if err != nil {
		return "", err
	}

	// IV должен быть уникальным, но не защищенным.
	// Поэтому обычно его включают в начало зашифрованного текста.
	if len(ciphertext) < aes.BlockSize {
		return "", ErrCiphertextTooShort
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream может работать на месте, если два аргумента совпадают.
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext), nil
}

// GenerateSecureToken Генерация случайного токена.
// Может быть использовано при генерации API ключа.
func GenerateSecureToken(length int) string {
	b := make([]byte, length)

	if _, err := rand.Read(b); err != nil {
		return ""
	}

	return hex.EncodeToString(b)
}
