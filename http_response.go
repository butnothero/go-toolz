package gt

type J map[string]any

type JResponse struct {
	Message any `json:"message"`
	Error   any `json:"error"`
	Payload any `json:"payload"`
}

// JMsg Сообщение.
func JMsg(message any) *JResponse {
	return &JResponse{
		Message: message,
		Error:   nil,
		Payload: nil,
	}
}

// JErr Ошибка.
func JErr(err any) *JResponse {
	return &JResponse{
		Message: MsgError,
		Error:   err,
		Payload: nil,
	}
}

// JValidationError Ошибка "валидации"
func JValidationError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgValidationError,
			Error:   MsgValidationError,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgValidationError,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JInternalServerError Ошибка "внутренняя ошибка сервера".
func JInternalServerError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgInternalServerError,
			Error:   MsgInternalServerError,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgInternalServerError,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JUnauthorizedError Ошибка "не авторизован".
func JUnauthorizedError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgUnauthorized,
			Error:   MsgUnauthorized,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgUnauthorized,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JBlockedError Ошибка "заблокировано".
func JBlockedError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgBlocked,
			Error:   MsgBlocked,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgBlocked,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JDuplicateError Ошибка "дубликат".
func JDuplicateError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgDuplicate,
			Error:   MsgDuplicate,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgDuplicate,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JNotFoundError Ошибка "не найдено".
func JNotFoundError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgNotFound,
			Error:   MsgNotFound,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgNotFound,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JInvalidCredentialsError Ошибка "неправильные учетные данные".
func JInvalidCredentialsError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgInvalidCredentials,
			Error:   MsgInvalidCredentials,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgInvalidCredentials,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JNoPermissionError Ошибка "нет доступа".
func JNoPermissionError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgNoPermission,
			Error:   MsgNoPermission,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgNoPermission,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JInvalidRefreshTokenError Ошибка "неправильный refresh токен".
func JInvalidRefreshTokenError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgInvalidRefreshToken,
			Error:   MsgInvalidRefreshToken,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgInvalidRefreshToken,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JInvalidAccessTokenError Ошибка "неправильный access токен".
func JInvalidAccessTokenError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgInvalidAccessToken,
			Error:   MsgInvalidAccessToken,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgInvalidAccessToken,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JInvalidDataError Ошибка "некорректные данные".
func JInvalidDataError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgInvalidData,
			Error:   MsgInvalidData,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgInvalidData,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JBadRequestError Ошибка "неправильный запрос".
func JBadRequestError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgBadRequest,
			Error:   MsgBadRequest,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgBadRequest,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JForbiddenError Ошибка "запрещено".
func JForbiddenError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgForbidden,
			Error:   MsgForbidden,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgForbidden,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JConflictError Ошибка "конфликт".
func JConflictError(err any, payload ...any) *JResponse {
	if err == nil {
		return &JResponse{
			Message: MsgConflict,
			Error:   MsgConflict,
			Payload: OptionalArg(payload),
		}
	}

	return &JResponse{
		Message: MsgConflict,
		Error:   err,
		Payload: OptionalArg(payload),
	}
}

// JMsgErr Ошибка с сообщением.
func JMsgErr(message, err any) *JResponse {
	return &JResponse{
		Message: message,
		Error:   err,
		Payload: nil,
	}
}

// JMsgPayload Успешно с сообщением и полезной нагрузкой.
func JMsgPayload(message, payload any) *JResponse {
	return &JResponse{
		Message: message,
		Error:   nil,
		Payload: payload,
	}
}

// JPayload Успешно с полезной нагрузкой.
func JPayload(payload any) *JResponse {
	return &JResponse{
		Message: MsgOK,
		Error:   nil,
		Payload: payload,
	}
}

// JOk Успешно.
func JOk(payload ...any) *JResponse {
	return &JResponse{
		Message: MsgOK,
		Error:   nil,
		Payload: OptionalArg(payload),
	}
}

// JCreated Создано.
func JCreated(payload ...any) *JResponse {
	return &JResponse{
		Message: MsgCreated,
		Error:   nil,
		Payload: OptionalArg(payload),
	}
}

// JUpdated Обновлено.
func JUpdated(payload ...any) *JResponse {
	return &JResponse{
		Message: MsgUpdated,
		Error:   nil,
		Payload: OptionalArg(payload),
	}
}

// JDeleted Удалено.
func JDeleted(payload ...any) *JResponse {
	return &JResponse{
		Message: MsgDeleted,
		Error:   nil,
		Payload: OptionalArg(payload),
	}
}

// JRes Полная настройка ответа.
func JRes(message, payload, err any) *JResponse {
	return &JResponse{
		Message: message,
		Error:   err,
		Payload: payload,
	}
}
