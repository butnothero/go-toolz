package gt

import (
	"fmt"
	"math"
	"math/rand"
	"strings"
	"time"
	"unicode/utf8"
)

var (
	randomizer = NewRandomizer()
)

type RandomizerConfig struct {
	Chars string
	Seed  int64
}

type Randomizer struct {
	core     *rand.Rand
	seed     int64
	chars    []rune
	charsLen int64
}

func NewRandomizer(conf ...RandomizerConfig) *Randomizer {
	var (
		chars = CharsDefault
		seed  = time.Now().UnixNano()
	)

	if len(conf) > 0 {
		config := conf[0]

		if config.Chars != "" {
			chars = config.Chars
		}

		if config.Seed > 0 {
			seed = config.Seed
		}
	}

	return &Randomizer{
		core:     rand.New(rand.NewSource(seed)), //nolint:gosec // ignore
		seed:     seed,
		chars:    []rune(chars),
		charsLen: int64(len(chars)),
	}
}

func (r *Randomizer) Chars() string {
	return string(append(make([]rune, 0, r.charsLen), r.chars...))
}

func (r *Randomizer) RuneAtChars(n int) rune {
	return r.chars[n]
}

func (r *Randomizer) SetChars(chars string) {
	r.chars = []rune(chars)
	r.charsLen = int64(len(r.chars))
}

func (r *Randomizer) Seed(seed int64) {
	r.core = rand.New(rand.NewSource(seed)) //nolint:gosec // ignore
}

func (r *Randomizer) WithChars(chars string) *Randomizer {
	return &Randomizer{
		core:     r.core,
		seed:     r.seed,
		chars:    []rune(chars),
		charsLen: int64(utf8.RuneCountInString(chars)),
	}
}

func (r *Randomizer) WithSeed(seed int64) *Randomizer {
	return &Randomizer{
		core:     rand.New(rand.NewSource(seed)), //nolint:gosec // ignore
		seed:     seed,
		chars:    r.chars,
		charsLen: r.charsLen,
	}
}

func (r *Randomizer) Bytes(size ...int) []byte {
	defaultValue := 10
	s := OptionalArg(size, defaultValue)
	buf := make([]byte, s)

	r.core.Read(buf)

	return buf
}

func (r *Randomizer) Rune() rune {
	return r.chars[r.core.Int63()%r.charsLen]
}

func (r *Randomizer) Runes(size ...int) []rune {
	defaultValue := 10
	s := OptionalArg(size, defaultValue)
	buf := make([]rune, s)

	for i := 0; i < s; i++ {
		buf[i] = r.chars[r.core.Int63()%r.charsLen]
	}

	return buf
}

func (r *Randomizer) String(size ...int) string {
	defaultValue := 10
	s := OptionalArg(size, defaultValue)

	return string(r.Runes(s))
}

func (r *Randomizer) LowerString(size ...int) string {
	defaultValue := 10
	s := OptionalArg(size, defaultValue)

	return strings.ToLower(randomizer.String(s))
}

func (r *Randomizer) IntArea(minValue, maxValue int) int {
	return minValue + (r.core.Int() % (maxValue - minValue))
}

func (r *Randomizer) Int() int {
	return r.core.Int()
}

func (r *Randomizer) Int32() int32 {
	return r.core.Int31()
}

func (r *Randomizer) Int64() int64 {
	return r.core.Int63()
}

func (r *Randomizer) Uint32() uint32 {
	return r.core.Uint32()
}

func (r *Randomizer) Uint64() uint64 {
	return r.core.Uint64()
}

func (r *Randomizer) UInt() uint {
	if Is64Bit {
		return uint(r.core.Uint64())
	}

	return uint(r.core.Uint32())
}

func (r *Randomizer) Float32() float32 {
	return r.core.Float32()
}

func (r *Randomizer) FastFloat32() float32 {
	return float32(r.core.Int63()%math.MaxInt32) / Float32ofMaxInt32
}

func (r *Randomizer) Float64() float64 {
	return r.core.Float64()
}

func (r *Randomizer) FastFloat64() float64 {
	return float64(r.core.Int63()) / Float64ofMaxInt64
}

func (r *Randomizer) Email() string {
	size := 10
	return fmt.Sprintf("%s@email.com", randomizer.LowerString(size))
}

func (r *Randomizer) EmailCfg(domain string, size ...int) string {
	defaultValue := 10
	s := OptionalArg(size, defaultValue)

	return fmt.Sprintf("%s@%s", randomizer.LowerString(s), domain)
}

func (r *Randomizer) Bool() bool {
	return r.Int()%2 == 0
}

func RandomWithChars(chars string) *Randomizer {
	return randomizer.WithChars(chars)
}

func RandomWithSeed(seed int64) *Randomizer {
	return randomizer.WithSeed(seed)
}

func RandomBytes(size ...int) []byte {
	return randomizer.Bytes(size...)
}

func RandomBytesF(size ...int) func() []byte {
	return func() []byte {
		return randomizer.Bytes(size...)
	}
}

func RandomRune() rune {
	return randomizer.Rune()
}

func RandomRuneF() func() rune {
	return func() rune {
		return randomizer.Rune()
	}
}

func RandomRunes(size ...int) []rune {
	return randomizer.Runes(size...)
}

func RandomRunesF(size ...int) func() []rune {
	return func() []rune {
		return randomizer.Runes(size...)
	}
}

func RandomIntArea(minValue, maxValue int) int {
	return randomizer.IntArea(minValue, maxValue)
}

func RandomIntAreaF(minValue, maxValue int) func() int {
	return func() int {
		return randomizer.IntArea(minValue, maxValue)
	}
}

func RandomString(size ...int) string {
	return randomizer.String(size...)
}

func RandomLowerString(size ...int) string {
	return randomizer.LowerString(size...)
}

func RandomStringF(size ...int) func() string {
	return func() string {
		return randomizer.String(size...)
	}
}

func RandomInt() int {
	return randomizer.Int()
}

func RandomInt32() int32 {
	return randomizer.Int32()
}

func RandomInt64() int64 {
	return randomizer.Int64()
}

func RandomUint32() uint32 {
	return randomizer.Uint32()
}

func RandomUint64() uint64 {
	return randomizer.Uint64()
}

func RandomUint() uint {
	return randomizer.UInt()
}

func RandomFloat32() float32 {
	return randomizer.Float32()
}

func RandomFastFloat32() float32 {
	return randomizer.FastFloat32()
}

func RandomFloat64() float64 {
	return randomizer.Float64()
}

func RandomChoice[T any](items []T) T {
	return items[RandomInt()%len(items)]
}

func RandomEmail() string {
	size := 10
	domain := "email.com"

	return randomizer.EmailCfg(domain, size)
}

func RandomEmailCfg(domain string, size ...int) string {
	return randomizer.EmailCfg(domain, size...)
}

func RandomBool() bool {
	return randomizer.Bool()
}
