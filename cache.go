package gt

import (
	"math"
	"sync"
	"time"
)

const (
	TTLCacheDefault = time.Second * 120
)

// CacheItem представляет элемент, хранящийся в кэше, с соответствующим ему TTL (Time To Live).
type CacheItem[T any] struct {
	value  T
	expiry time.Time
}

// Cache представляет собой хранилище значений ключей в памяти с поддержкой истечения срока действия.
type Cache[K comparable, T any] struct {
	// хранит элементы кэша
	data map[K]CacheItem[T]

	// управление параллельным доступом
	mu sync.RWMutex
}

// NewCache создает и инициализирует новый экземпляр Cache.
func NewCache[K comparable, T any]() *Cache[K, T] {
	return &Cache[K, T]{
		data: make(map[K]CacheItem[T]),
	}
}

func (c *Cache[K, T]) set(key K, value T, ttl time.Duration) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.data[key] = CacheItem[T]{
		value:  value,
		expiry: time.Now().Add(ttl),
	}
}

// Set добавляет или обновляет пару ключ-значение в кэше с максимально возможным TTL.
func (c *Cache[K, T]) Set(key K, value T) {
	c.set(key, value, math.MaxInt64)
}

// SetWithTTL добавляет или обновляет пару ключ-значение в кэше с заданным TTL.
func (c *Cache[K, T]) SetWithTTL(key K, value T, ttl time.Duration) {
	c.set(key, value, ttl)
}

// SetWithDefaultTTL добавляет или обновляет пару ключ-значение в кэше с TTL по-умолчанию.
func (c *Cache[K, T]) SetWithDefaultTTL(key K, value T) {
	c.set(key, value, TTLCacheDefault)
}

// Get извлекает значение, связанное с данным ключом, из кэша.
// Он также проверяет срок действия и удаляет элементы с истекшим сроком действия.
func (c *Cache[K, T]) Get(key K) (T, bool) {
	c.mu.Lock()
	defer c.mu.Unlock()

	item, ok := c.data[key]

	if !ok {
		return ZeroVal[T](), false
	}

	// запись найдена - проверяем истек ли срок действия
	if item.expiry.Before(time.Now()) {
		// удаляем запись из кэша, если истек срок ее действия
		delete(c.data, key)
		return ZeroVal[T](), false
	}

	return item.value, true
}

// Delete удаляет пару ключ-значение из кэша.
func (c *Cache[K, T]) Delete(key K) {
	c.mu.Lock()
	defer c.mu.Unlock()

	delete(c.data, key)
}

// Clear удаляет все пары ключ-значение из кэша.
func (c *Cache[K, T]) Clear() {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.data = make(map[K]CacheItem[T])
}
