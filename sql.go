package gt

import (
	"fmt"
	"strings"
)

// AddTableAliasToFields Добавить alias таблицы к каждому получаемому полю.
func AddTableAliasToFields(alias string, fields []string) []string {
	_fields := make([]string, len(fields))
	copy(_fields, fields)

	for i := range _fields {
		_fields[i] = fmt.Sprintf("%s.%s", alias, _fields[i])
	}

	return _fields
}

// AddTableAliasToFieldsWithJoin Добавить alias таблицы к каждому получаемому полю, а затем объединить в одну строку.
func AddTableAliasToFieldsWithJoin(alias string, fields []string, separator ...string) string {
	return strings.Join(AddTableAliasToFields(alias, fields), OptionalArg(separator, ","))
}
