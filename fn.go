package gt

import (
	"reflect"
)

// OptionalArg Позволяет получить необязательный аргумент функции,
// если такой есть. Можно передать значение по-умолчанию.
func OptionalArg[T any](arg []T, defaultValue ...T) T {
	var result T

	if defaultValue != nil {
		result = OptionalArg[T](defaultValue)
	}

	if arg == nil {
		return result
	}

	if len(arg) > 0 && !reflect.ValueOf(&arg[0]).Elem().IsZero() {
		return arg[0]
	}

	return result
}

// OptionalArgWithFn Позволяет выполнить функцию,
// если передан необязательный аргумент или значение по-умолчанию.
func OptionalArgWithFn[T any](arg []T, fn func(arg T), defaultValue ...T) T {
	opt := OptionalArg[T](arg, OptionalArg[T](defaultValue))

	if !reflect.ValueOf(&opt).Elem().IsZero() {
		fn(opt)
	}

	return opt
}
