package gt_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestDuration(t *testing.T) {
	testCases := map[string]struct {
		daysNumber float64
		check      func()
	}{
		"OK": {
			daysNumber: 10,
		},

		"Duration negative": {
			daysNumber: -10,
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(i, func(t *testing.T) {
			t.Parallel()

			expectedResult := time.Duration(tc.daysNumber*24) * time.Hour
			result := gt.Duration(0, 0, int(tc.daysNumber))

			require.Equal(t, result, expectedResult)
		})
	}
}
