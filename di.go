package gt

import (
	"github.com/samber/do"
)

var defaultDInjector *do.Injector

// NewDInjector Создает глобальный di контейнер.
// Использование глобального di контейнера не рекомендуется.
func NewDInjector() {
	defaultDInjector = do.New()
}

// getDefaultInjector Получить di контейнер по-умолчанию или из аргумента.
func getDefaultInjector(optionInjector []*do.Injector) *do.Injector {
	return OptionalArg(optionInjector, defaultDInjector)
}

// GetDInjector Получить di контейнер.
func GetDInjector() *do.Injector {
	return defaultDInjector
}

// DInvoke Получить экземпляр объекта из di контейнера.
func DInvoke[T any](customInjector ...*do.Injector) (T, error) {
	return do.Invoke[T](getDefaultInjector(customInjector))
}

// DMustInvoke Получить экземпляр объекта из di контейнера, который точно инициализирован.
func DMustInvoke[T any](customInjector ...*do.Injector) T {
	return do.MustInvoke[T](getDefaultInjector(customInjector))
}

// DInvokeNamed Получить экземпляр объекта по его имени из di контейнера.
func DInvokeNamed[T any](name string, customInjector ...*do.Injector) (T, error) {
	return do.InvokeNamed[T](getDefaultInjector(customInjector), name)
}

// DMustInvokeNamed Получить экземпляр объекта, который точно инициализирован, по его имени из di контейнера.
func DMustInvokeNamed[T any](name string, customInjector ...*do.Injector) T {
	return do.MustInvokeNamed[T](getDefaultInjector(customInjector), name)
}

// dProvide Зарегистрировать объект, используя функцию.
func dProvide[T any](fn func(i *do.Injector) (T, error), customInjector ...*do.Injector) {
	do.Provide(getDefaultInjector(customInjector), fn)
}

// DProvideNamedObject Зарегистрировать объект, используя ключевое слово.
func DProvideNamedObject[T any](name string, obj T, customInjector ...*do.Injector) {
	do.ProvideNamedValue[T](getDefaultInjector(customInjector), name, obj)
}

// DProvideNamedFn Зарегистрировать функцию, используя ключевое слово.
func DProvideNamedFn[T any](name string, fn func() (T, error), customInjector ...*do.Injector) {
	do.ProvideNamed[T](getDefaultInjector(customInjector), name, func(_ *do.Injector) (T, error) {
		return fn()
	})
}

// DProvideNamedFnWithArg Зарегистрировать функцию принимающую аргумент, используя ключевое слово.
func DProvideNamedFnWithArg[T any, K any](name string, fn func(K) (T, error), fnArg K, customInjector ...*do.Injector) {
	do.ProvideNamed[T](getDefaultInjector(customInjector), name, func(_ *do.Injector) (T, error) {
		return fn(fnArg)
	})
}

// DProvideFn Зарегистрировать объект через функцию.
func DProvideFn[T any](fn func() (T, error), customInjector ...*do.Injector) {
	dProvide[T](func(_ *do.Injector) (T, error) {
		return fn()
	}, getDefaultInjector(customInjector))
}

// DProvideFnWithArg Зарегистрировать объект через функцию, принимающую аргумент.
func DProvideFnWithArg[T any, K any](fn func(K) (T, error), fnArg K, customInjector ...*do.Injector) {
	dProvide[T](func(_ *do.Injector) (T, error) {
		return fn(fnArg)
	}, getDefaultInjector(customInjector))
}

// DProvideObject Зарегистрировать объект.
func DProvideObject[T any](obj T, customInjector ...*do.Injector) {
	dProvide[T](func(_ *do.Injector) (T, error) {
		return obj, nil
	}, getDefaultInjector(customInjector))
}

// dOverride Перезаписать объект, используя функцию.
func dOverride[T any](fn func(i *do.Injector) (T, error), customInjector ...*do.Injector) {
	do.Override[T](getDefaultInjector(customInjector), fn)
}

// DOverrideNamedFn Перезаписать функцию, используя ключевое слово.
func DOverrideNamedFn[T any](name string, fn func() (T, error), customInjector ...*do.Injector) {
	do.OverrideNamed[T](getDefaultInjector(customInjector), name, func(_ *do.Injector) (T, error) {
		return fn()
	})
}

// DOverrideNamedFnWithArg Перезаписать функцию принимающую аргумент, используя ключевое слово.
func DOverrideNamedFnWithArg[T any, K any](name string, fn func(K) (T, error), fnArg K, customInjector ...*do.Injector) {
	do.OverrideNamed[T](getDefaultInjector(customInjector), name, func(_ *do.Injector) (T, error) {
		return fn(fnArg)
	})
}

// DOverrideFn Перезаписать объект через функцию.
func DOverrideFn[T any](constructor func() (T, error), customInjector ...*do.Injector) {
	dOverride[T](func(_ *do.Injector) (T, error) {
		return constructor()
	}, getDefaultInjector(customInjector))
}

// DOverrideFnWithArg Перезаписать объект через функцию, принимающую аргумент.
func DOverrideFnWithArg[T any, K any](fn func(K) (T, error), fnArg K, customInjector ...*do.Injector) {
	dOverride[T](func(_ *do.Injector) (T, error) {
		return fn(fnArg)
	}, getDefaultInjector(customInjector))
}

// DOverrideObject Перезаписать объект.
func DOverrideObject[T any](obj T, customInjector ...*do.Injector) {
	dOverride[T](func(_ *do.Injector) (T, error) {
		return obj, nil
	}, getDefaultInjector(customInjector))
}

// DOverrideNamedObject Перезаписать объект, используя ключевое слово.
func DOverrideNamedObject[T any](name string, obj T, customInjector ...*do.Injector) {
	do.OverrideNamedValue[T](getDefaultInjector(customInjector), name, obj)
}
