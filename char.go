package gt

const (
	CharsRussianLettersUpper = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
	CharsRussianLettersLower = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
	CharsRussianLetters      = CharsRussianLettersLower + CharsRussianLettersUpper
	CharsRomanLettersLower   = "abcdefghijklmnopqrstuvwxyz"
	CharsRomanLettersUpper   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	CharsRomanLetters        = CharsRomanLettersLower + CharsRomanLettersUpper
	CharsRomanNumerals       = "ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫ"
	CharsArabicNumerals      = "0123456789"
	CharsDefault             = CharsArabicNumerals + CharsRomanLetters
)
