package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestChan_IsChan(t *testing.T) {
	var (
		isChan  chan int
		notChan int
	)
	require.True(t, gt.IsChan(isChan), "Define chan variable as a non chan")
	require.False(t, gt.IsChan(notChan), "Define not chan variable as a chan")
}

func TestChan_IsClosed(t *testing.T) {
	var (
		openedChan = make(chan int)
		closedChan = make(chan int)
	)
	defer close(openedChan)
	close(closedChan)

	require.True(t, gt.ChanIsClosed(closedChan), "Define closed chan as opened")
	require.False(t, gt.ChanIsClosed(openedChan), "Define opened chan as closed")

	defer func() {
		if r := recover(); r == nil {
			t.Error("expected panic")
		}
	}()

	gt.ChanIsClosed(1)
}
