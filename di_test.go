package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestNewDInjector(t *testing.T) {
	gt.NewDInjector()
	require.NotNil(t, gt.GetDInjector())
}

func TestDProvideInvoke(t *testing.T) {
	var (
		num int64 = 123
		str       = "hello"
	)

	gt.DProvideObject(num)
	gt.DProvideObject(str)

	numInvoke, err := gt.DInvoke[int64]()
	require.NoError(t, err)
	require.Equal(t, num, numInvoke)

	strInvoke, err := gt.DInvoke[string]()
	require.NoError(t, err)
	require.Equal(t, str, strInvoke)
}

// TODO дописать тесты
