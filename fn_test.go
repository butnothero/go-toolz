package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestOptionalArg(t *testing.T) {
	t.Parallel()

	type SomeStruct struct {
		Name string
	}

	ss1 := SomeStruct{
		Name: "Hello",
	}

	ss2 := SomeStruct{
		Name: "World",
	}

	testCases := map[string]struct {
		arg          []any
		defaultValue any
		expected     any
	}{
		"[nil] Full nil": {
			arg:          nil,
			defaultValue: nil,
			expected:     nil,
		},
		"[string] Without Default Value": {
			arg:          []any{"hello"},
			defaultValue: "",
			expected:     "hello",
		},
		"[string] With Default Value": {
			arg:          []any{"hello"},
			defaultValue: "world",
			expected:     "hello",
		},
		"[string] Without arg": {
			arg:          nil,
			defaultValue: "world",
			expected:     "world",
		},
		"[string] Without arg and default value": {
			arg:          nil,
			defaultValue: "",
			expected:     "",
		},
		"[int] Without Default Value": {
			arg:          []any{1},
			defaultValue: "",
			expected:     1,
		},
		"[int] With Default Value": {
			arg:          []any{1},
			defaultValue: 2,
			expected:     1,
		},
		"[int] Without arg": {
			arg:          nil,
			defaultValue: 2,
			expected:     2,
		},
		"[int] Without arg and default value": {
			arg:          nil,
			defaultValue: 0,
			expected:     0,
		},
		"[struct] Without Default Value": {
			arg:          []any{ss1},
			defaultValue: nil,
			expected:     ss1,
		},
		"[struct] With Default Value": {
			arg:          []any{ss1},
			defaultValue: ss2,
			expected:     ss1,
		},
		"[struct] Without arg": {
			arg:          nil,
			defaultValue: ss2,
			expected:     ss2,
		},
		"[struct] Without arg and default value": {
			arg:          nil,
			defaultValue: SomeStruct{},
			expected:     SomeStruct{},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(i, func(t *testing.T) {
			t.Parallel()

			result := gt.OptionalArg(tc.arg, tc.defaultValue)

			require.Equal(t, tc.expected, result)
		})
	}
}
