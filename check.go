package gt

import (
	"reflect"
)

// AllFieldsInStructIsNil Проверить, что все поля в структуре являются nil.
func AllFieldsInStructIsNil(s any, ignoreNonPointer ...bool) bool {
	ignoreNP := OptionalArg(ignoreNonPointer, false)
	rvArg := reflect.ValueOf(s)
	kindArg := rvArg.Kind()

	var rv reflect.Value

	if kindArg == reflect.Ptr { //nolint:gocritic // ignore
		rv = rvArg.Elem()

		if rv.Kind() != reflect.Struct {
			panic(ErrArgumentMustBeAStruct)
		}

		if rvArg.IsNil() {
			return true
		}
	} else if rvArg.Kind() != reflect.Struct {
		panic(ErrArgumentMustBeAStruct)
	} else {
		rv = reflect.ValueOf(s)
	}

	numField := rv.NumField()

	for i := 0; i < numField; i++ {
		field := rv.Field(i)

		if field.Kind() == reflect.Ptr {
			if !field.IsNil() {
				return false
			}
		} else if !ignoreNP {
			return false
		}
	}

	return true
}
