package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestToPointer(t *testing.T) {
	testCases := map[string]struct {
		value any
	}{
		"string": {
			value: "hello world",
		},
		"int": {
			value: 123,
		},
		"struct": {
			value: struct {
				name string
			}{},
		},
		"float64": {
			value: float64(123),
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(i, func(t *testing.T) {
			t.Parallel()
			require.Equal(t, tc.value, *gt.Ptr(tc.value))
		})
	}
}
