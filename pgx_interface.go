package gt

//go:generate mockgen -source=$GOFILE -destination=pgx_interface_mock.go -package=gt

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
)

type (
	PgxPool interface {
		Close()
		Acquire(ctx context.Context) (*pgxpool.Conn, error)
		AcquireFunc(ctx context.Context, f func(*pgxpool.Conn) error) error
		AcquireAllIdle(ctx context.Context) []*pgxpool.Conn
		Reset()
		Config() *pgxpool.Config
		Stat() *pgxpool.Stat
		Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
		Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
		QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
		SendBatch(ctx context.Context, b *pgx.Batch) pgx.BatchResults
		Begin(ctx context.Context) (pgx.Tx, error)
		BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error)
		CopyFrom(ctx context.Context, tableName pgx.Identifier, columnNames []string, rowSrc pgx.CopyFromSource) (int64, error)
		Ping(ctx context.Context) error
	}

	// PgxTx is an interface to work with pgx.Conn, pgxpool.Conn or pgxpool.Pool
	// StmtContext and Stmt are not implemented!
	PgxTx interface {
		Begin(ctx context.Context) (pgx.Tx, error)

		CopyFrom(ctx context.Context, tableName pgx.Identifier, columnNames []string, rowSrc pgx.CopyFromSource) (int64, error)
		SendBatch(ctx context.Context, b *pgx.Batch) pgx.BatchResults

		Exec(ctx context.Context, sql string, arguments ...any) (commandTag pgconn.CommandTag, err error)
		Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
		QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
	}
)
