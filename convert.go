package gt

import "reflect"

const (
	BInKB int64 = 1_024
	BInMB int64 = 1_048_576
	BInGB int64 = 1_073_741_824
)

// Convert Конвертировать значение с неизвестным типом данных в заданный тип данных.
// Аналог slice, ok := arg.([]string).
func Convert[T any](arg any) (result T, ok bool) {
	defer func() {
		if r := recover(); r != nil {
			return
		}
	}()

	val := reflect.ValueOf(arg)

	resultKind := reflect.ValueOf(result).Kind()

	if val.Kind() == resultKind {
		return arg.(T), true //nolint:errcheck // ignore
	}

	return result, false
}

// MBToBytes Конвертация МБ в байты
func MBToBytes(mb int64) int64 {
	return mb * BInMB
}
