package gt_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestNewSet(t *testing.T) {
	set := gt.NewSet[any]()

	require.Equal(
		t,
		reflect.TypeOf(set).Kind(),
		reflect.TypeOf(map[any]struct{}{}).Kind(),
		"Invalid set type",
	)
}

func TestSet_Add(t *testing.T) {
	set := gt.NewSet[any]()
	data := []struct {
		name string
		val  any
	}{
		{"string", "some"},
		{"int", 1},
		{"bool", true},
		{"float", 1.1},
	}

	for i := range data {
		item := data[i]
		set.Add(item.val)
		_, ok := set[item.val]
		require.Truef(t, ok, "Test add %s failed", item.name)
	}
}

// TODO Дописать тесты
//
//var TestDelete = []struct {
//	name string
//	val  any
//}{
//	{"string", "some"},
//	{"int", 1},
//}
//
//func TestSet_Delete(t *testing.T) {
//	for _, test := range TestDelete {
//		set.Delete(test.val)
//		_, ok := set[test.val]
//		require.Falsef(t, ok, "Test delete %s failed", test.name)
//	}
//}
//
//var TestHas = []struct {
//	name     string
//	val      any
//	expected bool
//}{
//	{"string", "some", false},
//	{"int", 1, false},
//	{"bool", true, true},
//	{"float", 1.1, true},
//}
//
//func TestSet_Has(t *testing.T) {
//	for _, test := range TestHas {
//		require.Equalf(t, test.expected, set.Has(test.val), "Test has %s failed", test.name)
//	}
//}
//
//func TestSet_Len(t *testing.T) {
//	require.Equal(t, 2, set.Len(), "Invalid set len")
//}
//
//func TestSet_ForEach(t *testing.T) {
//	set.ForEach(func(a any) {
//		require.True(t, set.Has(a), "Invalid func integration for each")
//	})
//}
//
//func TestSet_Clear(t *testing.T) {
//	set.Clear()
//	require.Equal(t, 0, set.Len(), "There are some values after clear function")
//}
