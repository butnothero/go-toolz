package gt

import "reflect"

// IdxInSlice Возвращает индекс первого вхождения v в s, или -1, если его нет.
func IdxInSlice[S ~[]E, E comparable](slice S, value E) int {
	for i := range slice {
		if value == slice[i] {
			return i
		}
	}

	return -1
}

// IdxInSliceFn Возвращает первый индекс i, удовлетворяющий f(s[i]), или -1,
// если ни один из них не удовлетворяет.
func IdxInSliceFn[S ~[]E, E any](slice S, f func(E) bool) int {
	for i := range slice {
		if f(slice[i]) {
			return i
		}
	}

	return -1
}

// SliceContains Присутствует ли v в s.
func SliceContains[S ~[]E, E comparable](slice S, value E) bool {
	return IdxInSlice(slice, value) >= 0
}

// SliceContainsFn Удовлетворяет ли хотя бы один
// элемент e из s f(e).
func SliceContainsFn[S ~[]E, E any](slice S, f func(E) bool) bool {
	return IdxInSliceFn(slice, f) >= 0
}

// ConcatSlices Объединяет несколько срезов в один срез и возвращает его.
func ConcatSlices[T any](slices [][]T) []T {
	var totalLen int

	for _, s := range slices {
		totalLen += len(s)
	}

	result := make([]T, totalLen)

	var i int

	for _, s := range slices {
		i += copy(result[i:], s)
	}

	return result
}

// SliceIsEmpty Проверить срез на пустоту.
func SliceIsEmpty[T any](slice []T) bool {
	if slice == nil {
		return true
	}

	if len(slice) == 0 {
		return true
	}

	return false
}

// IsSlice Проверить, является ли переданное значение срезом.
func IsSlice(v any) bool {
	return reflect.TypeOf(v).Kind() == reflect.Slice
}

// TransformSlice Создать новый срез с результатом вызова переданной функции для каждого элемента старого среза.
func TransformSlice[S ~[]E, E any, T any](slice S, f func(E) (T, error)) ([]T, error) {
	result := make([]T, 0, len(slice))

	for i := range slice {
		el, err := f(slice[i])

		if err != nil {
			return nil, err
		}

		result = append(result, el)
	}

	return result, nil
}

// FilterSlice возвращает отфильтрованный срез,
// где каждый элемент среза удовлетворяет условию переданной функции.
func FilterSlice[S ~[]E, E any](slice S, f func(E) bool) S {
	filtered := make(S, 0)

	for i := range slice {
		if f(slice[i]) {
			filtered = append(filtered, slice[i])
		}
	}

	return filtered
}

// FindInSlice возвращает элемент из среща,
// удовлетворяющий условию переданной функции.
func FindInSlice[S ~[]E, E any](slice S, f func(E) bool) (E, bool) {
	var (
		found E
		ok    bool
	)

	for i := range slice {
		if f(slice[i]) {
			found = slice[i]
			ok = true

			break
		}
	}

	return found, ok
}
