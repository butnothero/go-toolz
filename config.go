package gt

import (
	"path/filepath"
	"strings"

	"github.com/ory/viper"
)

// ReadConfigFromFile Считать конфигурацию из файла.
func ReadConfigFromFile[T any](pathToFile string) (*T, error) {
	if strings.TrimSpace(pathToFile) == "" {
		return nil, ErrPathIsEmpty
	}

	path := filepath.Dir(pathToFile)
	file := filepath.Base(pathToFile)
	split := strings.Split(file, ".")
	minLen := 2

	if len(split) < minLen {
		return nil, ErrInvalidFileName
	}

	fileName := strings.Join(split[:len(split)-1], ".")
	fileExt := split[len(split)-1]

	cfg := new(T)

	viper.AutomaticEnv()
	viper.AddConfigPath(path)
	viper.SetConfigType(fileExt)
	viper.SetConfigName(fileName)

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	err = viper.Unmarshal(&cfg)
	if err != nil {
		return nil, err
	}

	return cfg, err
}
