package gt

import (
	"errors"
	"fmt"
	"mime/multipart"
	"net/http"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.uber.org/zap"
)

type (
	EchoValidator struct {
		Core *validator.Validate
	}

	EchoValidationError struct {
		// Namespace string `json:"namespace"`
		Field   string `json:"field"`
		Tag     string `json:"tag"`
		Message string `json:"message"`
	}

	EchoValidationErrors []EchoValidationError
)

func NewEchoValidator(validate ...*validator.Validate) *EchoValidator {
	return &EchoValidator{
		Core: OptionalArg(validate, validator.New(validator.WithRequiredStructEnabled())),
	}
}

// Validate Проверка request body.
func (ev *EchoValidator) Validate(i any) error {
	if reflect.TypeOf(i).Kind() == reflect.Slice {
		err := ev.Core.Var(i, "min=1,dive")
		return err
	}

	if err := ev.Core.Struct(i); err != nil {
		vStruct := reflect.ValueOf(i)
		numField := vStruct.Type().NumField()
		fieldsMap := make(map[string]string, numField)
		structKeys := []string{"json", "param", "query", "form"}

		for idx := 0; idx < numField; idx++ {
			for iKey := range structKeys {
				f := FirstLetterToLower(vStruct.Type().Field(idx).Tag.Get(structKeys[iKey]))
				if f != "" {
					fieldsMap[vStruct.Type().Field(idx).Name] = f
					break
				}
			}
		}

		validationErrors := err.(validator.ValidationErrors) //nolint:errcheck // ignore

		var errs EchoValidationErrors

		for _, e := range validationErrors {
			field := fieldsMap[e.Field()]
			tag := e.Tag()

			errs = append(errs, EchoValidationError{
				Field:   field,
				Tag:     tag,
				Message: fmt.Sprintf("field validation for '%s' failed on the '%s' tag", field, tag),
			})
		}

		return errs
	}

	return nil
}

// Error Имплементация метода Error для глобального интерфейса error.
func (ve EchoValidationErrors) Error() string {
	sErrs := make([]string, len(ve))
	for i, validationError := range ve {
		sErrs[i] = validationError.Message
	}

	return strings.Join(sErrs, "\n")
}

// EchoMwZap Zap middleware для логов echo.
func EchoMwZap(l IZap) echo.MiddlewareFunc {
	return middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogValuesFunc: func(_ echo.Context, v middleware.RequestLoggerValues) error {
			if strings.Contains(v.URI, "swagger") {
				return nil
			}

			err := v.Error
			errStr := ""
			if err != nil {
				errStr = err.Error()
			}

			l.Infow("request",
				zap.String("level", "info"),
				zap.String("method", v.Method),
				zap.String("uri", v.URI),
				zap.Int("status", v.Status),
				zap.String("protocol", v.Protocol),
				zap.String("host", v.Host),
				zap.String("user-agent", v.UserAgent),
				zap.String("remote-ip", v.RemoteIP),
				zap.String("error", errStr),
				zap.String("content-length", v.ContentLength),
			)
			return nil
		},
		LogLatency:       true,
		LogProtocol:      true,
		LogRemoteIP:      true,
		LogHost:          true,
		LogMethod:        true,
		LogURI:           true,
		LogURIPath:       true,
		LogRoutePath:     true,
		LogRequestID:     true,
		LogReferer:       true,
		LogUserAgent:     true,
		LogStatus:        true,
		LogError:         true,
		LogContentLength: true,
		LogResponseSize:  true,
	})
}

// EchoFileHeader Получить файл из ec.FormFile.
func EchoFileHeader(ec echo.Context, fileName string, isRequired bool) (*multipart.FileHeader, error) {
	file, err := ec.FormFile(fileName)

	if errors.Is(err, http.ErrMissingFile) {
		if isRequired {
			return nil, err
		}

		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return file, nil
}

// EchoDTOSlice Проверить dto-срез.
func EchoDTOSlice[T any](ec echo.Context) (body []T, statusCode int, err error) {
	body = make([]T, 0)

	if err := ec.Bind(&body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if err := ec.Validate(body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	return body, http.StatusOK, nil
}

// EchoDTO Проверить dto-структуру.
func EchoDTO[T any](ec echo.Context) (body *T, statusCode int, err error) {
	body = new(T)

	if err := ec.Bind(body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	if err := ec.Validate(*body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	return body, http.StatusOK, nil
}
