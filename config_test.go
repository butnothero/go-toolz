package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

var TestNew = []struct {
	name        string
	path        string
	DataType    interface{}
	expectedErr bool
}{
	{"Ok", "internal/testfiles/test_conf.yml", map[string]interface{}{}, false},
	{"Empty Path", "", map[string]interface{}{}, true},
	{"Invalid Path", "/", map[string]interface{}{}, true},
	{"Invalid Config", "internal/testfiles/invalid_conf.txt", map[string]interface{}{}, true},
	{"Invalid type", "internal/testfiles/test_conf.yml", map[int]int{}, true},
}

func TestConfig_New(t *testing.T) {
	var (
		cfg any
		err error
	)
	for _, test := range TestNew {
		switch test.DataType.(type) {
		case map[string]interface{}:
			cfg, err = gt.ReadConfigFromFile[map[string]interface{}](test.path)
		case map[int]int:
			cfg, err = gt.ReadConfigFromFile[map[int]int](test.path)
		}

		if test.expectedErr {
			require.Error(t, err, test.name)
		} else {
			require.Nilf(t, err, "In test %s unexpected error: %s", test.name, err)
			require.NotNilf(t, cfg, "In test %s unexpected nil config", test.name)
		}
	}
}
