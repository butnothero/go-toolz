package gt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// MultipartFormData Создает объект multipart/form-data.
// Если в начале значения по ключу указан символ @, то
// данное значение будет расцениваться как путь к файлу,
// который надо записать в объект multipart/form-data.
func MultipartFormData(formData map[string]string) (string, io.Reader, error) {
	var err error

	fileSymbol := "@"
	contentType := ""
	body := new(bytes.Buffer)
	mp := multipart.NewWriter(body)

	defer func(mp *multipart.Writer) {
		err = mp.Close()

		if err != nil {
			contentType = ""
			body = nil
		}
	}(mp)

	for key, val := range formData {
		if strings.HasPrefix(val, fileSymbol) {
			err = func() error {
				val = val[1:]

				var file *os.File

				file, err = os.Open(val)

				if err != nil {
					return err
				}

				defer func(file *os.File) {
					err = file.Close()

					if err != nil {
						contentType = ""
						body = nil
					}
				}(file)

				var part io.Writer

				part, err = mp.CreateFormFile(key, filepath.Base(file.Name()))

				if err != nil {
					return err
				}

				_, err = io.Copy(part, file)

				if err != nil {
					return err
				}

				return nil
			}()

			if err != nil {
				return "", nil, err
			}

			continue
		}

		err = mp.WriteField(key, val)

		if err != nil {
			return "", nil, err
		}
	}

	contentType = mp.FormDataContentType()

	return contentType, body, err
}

// MultipartFileHeader Создать объект заголовка файла,
// который описывает файловую часть multipart запроса.
func MultipartFileHeader(filePath string, maxMemory int64, fieldName ...string) (*multipart.FileHeader, error) {
	var (
		fileHeader   *multipart.FileHeader
		err          error
		file         *os.File
		fieldNameArg = OptionalArg(fieldName, "file")
	)

	file, err = os.Open(filePath)

	if err != nil {
		return nil, err
	}

	defer func(file *os.File) {
		err = file.Close()

		if err != nil {
			fileHeader = nil
		}
	}(file)

	var (
		buff     bytes.Buffer
		formPart io.Writer
	)

	buffWriter := io.Writer(&buff)
	formWriter := multipart.NewWriter(buffWriter)

	formPart, err = formWriter.CreateFormFile(fieldNameArg, filepath.Base(file.Name()))

	if err != nil {
		return nil, err
	}

	if _, err = io.Copy(formPart, file); err != nil {
		return nil, err
	}

	err = formWriter.Close()

	if err != nil {
		return nil, err
	}

	buffReader := bytes.NewReader(buff.Bytes())
	formReader := multipart.NewReader(buffReader, formWriter.Boundary())

	var multipartForm *multipart.Form
	multipartForm, err = formReader.ReadForm(maxMemory)

	if err != nil {
		return nil, err
	}

	files, exists := multipartForm.File[fieldNameArg]

	if !exists || len(files) == 0 {
		return nil, fmt.Errorf("'%v' does not exist", fieldNameArg)
	}

	fileHeader = files[0]

	return fileHeader, err
}

// RawMessageDTO Проверить dto-структуру, представленную как json.RawMessage.
func RawMessageDTO[T any](payload json.RawMessage, validator ...ISimpleValidator) (body *T, statusCode int, err error) {
	body = new(T)

	if err := json.Unmarshal(payload, &body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	v := OptionalArg[ISimpleValidator](validator, NewEchoValidator())

	if err := v.Validate(*body); err != nil {
		return nil, http.StatusBadRequest, err
	}

	return body, http.StatusOK, nil
}
