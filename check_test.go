package gt_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	gt "gitlab.com/butnothero/go-toolz/v2"
)

func TestAllFieldsInStructIsNil(t *testing.T) {
	testCases := map[string]struct {
		data             any
		ignoreNonPointer bool
		expect           bool
		expectPanic      bool
	}{
		"Nil struct fields": {
			data: struct {
				name *string
			}{},
			expect: true,
		},
		"Not nil struct fields": {
			data: struct {
				name *string
			}{
				name: gt.Ptr("World"),
			},
			expect: false,
		},
		"Empty struct": {
			data:   struct{}{},
			expect: true,
		},
		"Empty struct and ignore non pointer": {
			data:             struct{}{},
			ignoreNonPointer: true,
			expect:           true,
		},
		"Struct without pointer field": {
			data: struct {
				name string
			}{
				name: "World",
			},
			expect: false,
		},
		"Struct without pointer field and ignore non pointer": {
			data: struct {
				name string
			}{
				name: "World",
			},
			ignoreNonPointer: true,
			expect:           true,
		},
		"Struct with pointer and common field": {
			data: struct {
				name  string
				nameP *string
			}{
				name:  "World",
				nameP: gt.Ptr("World"),
			},
			expect: false,
		},
		"Struct with pointer and common field 2": {
			data: struct {
				name  string
				nameP *string
			}{
				name:  "World",
				nameP: nil,
			},
			expect: false,
		},
		"Struct with 2 pointer fields is nil": {
			data: struct {
				name  *string
				name2 *string
			}{},
			expect: true,
		},
		"Struct with 2 pointer fields is nil and ignore non pointer": {
			data: struct {
				name  *string
				name2 *string
			}{},
			ignoreNonPointer: true,
			expect:           true,
		},
		"Struct with 2 pointer fields": {
			data: struct {
				name  *string
				name2 *string
			}{
				name:  gt.Ptr("World"),
				name2: nil,
			},
			expect: false,
		},
		"Struct with 2 pointer fields and ignore non pointer": {
			data: struct {
				name  *string
				name2 *string
			}{
				name:  gt.Ptr("World"),
				name2: nil,
			},
			ignoreNonPointer: true,
			expect:           false,
		},
		"Pointer struct (new)": {
			data: gt.Ptr(struct {
				name *string
			}{
				name: gt.Ptr("World"),
			}),
			expect: false,
		},
		"Pointer struct (new) with nil field": {
			data: gt.Ptr(struct {
				name *string
			}{}),
			expect: true,
		},
		"Pointer struct (without new)": {
			data: &struct {
				name *string
			}{
				name: gt.Ptr("World"),
			},
			expect: false,
		},
		"Pointer struct (without new) with nil field": {
			data: &struct {
				name *string
			}{},
			expect: true,
		},
		"Pointer struct (without new) with nil field and ignore non pointer": {
			data: &struct {
				name  *string
				name2 string
			}{},
			ignoreNonPointer: true,
			expect:           true,
		},
		"Slice panic": {
			data:        []string{},
			expectPanic: true,
		},
		"Nil panic": {
			data:        nil,
			expectPanic: true,
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(i, func(t *testing.T) {
			defer func() {
				err := recover()

				if !tc.expectPanic && err == nil {
					return
				}

				if tc.expectPanic && err != nil {
					return
				}

				t.Error(err)
			}()

			require.Equal(t, tc.expect, gt.AllFieldsInStructIsNil(tc.data, tc.ignoreNonPointer))
		})
	}
}
