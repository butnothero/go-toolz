package gt

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

func NewPgxPool(ctx context.Context, dbString string) (*pgxpool.Pool, error) {
	poolConn, err := pgxpool.New(ctx, dbString)

	if err != nil {
		return nil, err
	}

	err = poolConn.Ping(ctx)

	if err != nil {
		return nil, err
	}

	return poolConn, nil
}
