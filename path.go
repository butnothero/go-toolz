package gt

import (
	"runtime"
	"strings"
)

// CallerPath Путь до файла где была вызвана функция, включая сам файл.
// Эквивалент __filename в NodeJS.
func CallerPath(skip ...int) string {
	_, filename, _, _ := runtime.Caller(OptionalArg(skip, 1)) //nolint:dogsled // ignore

	return filename
}

// ReplaceIdxInPath Заменить последний элемент в пути.
func ReplaceIdxInPath(path, newEnd string, idx int) string {
	split := strings.Split(path, "/")

	if len(split) <= 1 {
		return newEnd
	}

	path = strings.Join(split[:len(split)-idx], "/") + newEnd

	return path
}
