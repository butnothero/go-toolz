# Установка/обновление зависимостей
.PHONY: init
init:
	go mod tidy
	go mod download

# mockgen
	go install go.uber.org/mock/mockgen@latest

# golangci-lint
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

# Запуск модульных тестов
.PHONY: unit_test
unit_test:
	go test -v -cover ./...

# Форматировать код
.PHONY: fmt
fmt:
	go fmt ./

# Запустить линтер
.PHONY: lint
lint:
	golangci-lint -v run --fix ./...

# Генерация
.PHONY: generate
generate:
	go generate ./...
