package gt

import (
	"context"

	"github.com/jackc/pgx/v5"
	"go.uber.org/multierr"
)

type CtxKeyPgxTx any

var defaultCtxKeyPgxTx = new(CtxKeyPgxTx)

type PgxManagerLogger interface {
	Error(error)
	Named(string) PgxManagerLogger
}

type PgxManagerOption func(m *PgxManager)

func PgxManagerWithLogger(logger PgxManagerLogger) PgxManagerOption {
	return func(m *PgxManager) {
		m.l = logger.Named("PgxManager")
	}
}

type PgxManager struct {
	pool     PgxPool
	ctxTxKey CtxKeyPgxTx
	l        PgxManagerLogger
}

func NewPgxManager(pool PgxPool, opts ...PgxManagerOption) *PgxManager {
	m := &PgxManager{
		pool:     pool,
		ctxTxKey: defaultCtxKeyPgxTx,
	}

	for _, opt := range opts {
		opt(m)
	}

	return m
}

func (m *PgxManager) Do(ctx context.Context, fn func(ctx context.Context) error) error {
	var (
		_ctx context.Context
		err  error
	)

	_, ok := ctx.Value(m.ctxTxKey).(pgx.Tx)

	if !ok {
		_ctx, err = BeginPgxTx(ctx, m.ctxTxKey, m.pool)

		if err != nil {
			m.tryErrLog(err)
			return err
		}
	}

	defer func() {
		endTxErr := EndPgxTx(_ctx, m.ctxTxKey, err)

		if endTxErr != nil {
			err = multierr.Combine(err, endTxErr)
			m.tryErrLog(err)
		}
	}()

	err = fn(_ctx)

	return err
}

func (m *PgxManager) tryErrLog(err error) {
	if m.l == nil {
		return
	}

	m.l.Error(err)
}
