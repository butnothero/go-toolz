package gt

import (
	"fmt"
	"reflect"
	"unsafe"
)

type cloneConfig struct {
	expectedPointersCount int
	unexportedStrategy    unexportedCloneStrategy
	errOnUnsupported      bool
}

type cloneFuncOptions func(*cloneConfig)

// CloneWithExpectedPtrsCount - это функциональная опция для функции Clone, которая устанавливает начальную емкость для pointers map, используемой в процессе клонирования.
// Если количество указателей в исходном объекте известно и достаточно велико, этот параметр может сократить объем выделяемых ресурсов и повысить производительность.
func CloneWithExpectedPtrsCount(cnt int) func(*cloneConfig) {
	return func(c *cloneConfig) {
		c.expectedPointersCount = cnt
	}
}

type unexportedCloneStrategy int

const (
	shallowCopyUnexportedStrategy unexportedCloneStrategy = iota
	forceDeepCopyUnexportedStrategy
	forceZeroUnexported
)

// CloneWithForceUnexported - это функциональная опция для функции Clone. Если этот параметр включен, не экспортированные поля будут принудительно клонированы.
func CloneWithForceUnexported() func(*cloneConfig) {
	return func(c *cloneConfig) {
		c.unexportedStrategy = forceDeepCopyUnexportedStrategy
	}
}

// CloneWithZeroUnexported - это функциональная опция для функции Clone. Если этот параметр включен, для не экспортированных полей будет принудительно установлено нулевое значение.
func CloneWithZeroUnexported() func(*cloneConfig) {
	return func(c *cloneConfig) {
		c.unexportedStrategy = forceZeroUnexported
	}
}

// CloneWithErrOnUnsupported - это функциональная опция для функции клонирования. Если этот параметр включен, попытка клонирования каналов и функций, даже если они являются вложенными, приведет к ошибке.
func CloneWithErrOnUnsupported() func(*cloneConfig) {
	return func(c *cloneConfig) {
		c.errOnUnsupported = true
	}
}

// cloneContext содержит настройки и указатели на сопоставление значений для одного клона
type cloneContext struct {
	ptrs               map[unsafe.Pointer]reflect.Value
	unexportedStrategy unexportedCloneStrategy
	errOnUnsupported   bool
}

func (ctx *cloneContext) resolvePointer(ptr unsafe.Pointer) (reflect.Value, bool) {
	v, ok := ctx.ptrs[ptr]
	return v, ok
}

func (ctx *cloneContext) setPointer(ptr unsafe.Pointer, value reflect.Value) {
	ctx.ptrs[ptr] = value
}

// Clone возвращает полную копию переданного аргумента. Клонированное значение будет идентично исходному значению.
// Клонированные указатели будут указывать на новый объект с тем же значением, что и в источнике.
// Это также работает для вложенных значений, таких как части коллекций (array, slice и map) или поля переданных структур.
// Это дополнительно гарантирует, что если два или более указателя на исходный объект ссылаются на одно и то же значение, указатели на клоны будут ссылаться на одно и то же значение,
// но если два или более фрагмента исходного объекта ссылаются на одну и ту же область памяти (т.е. на один и тот же базовый массив), копия будет ссылаться на разные массивы.
// По-умолчанию не экспортированные поля структур будут копироваться неглубоко, но это можно изменить с помощью функциональных опций.
// Неподдерживаемые типы (chan и func) по умолчанию будут копироваться неглубоко, но это также можно изменить с помощью функциональных опций.
// Небезопасные указатели будут обрабатываться как обычные значения.
func Clone[T any](src T, opts ...cloneFuncOptions) (T, error) {
	cfg := cloneConfig{}

	for _, o := range opts {
		o(&cfg)
	}

	// cloneCtx будет передан любому вложенному объекту clone с рекурсивным вызовом.
	// Он содержит настройки и указатели на сопоставление значений
	cloneCtx := &cloneContext{
		ptrs:               make(map[unsafe.Pointer]reflect.Value, cfg.expectedPointersCount),
		unexportedStrategy: cfg.unexportedStrategy,
		errOnUnsupported:   cfg.errOnUnsupported,
	}

	// Поверхностная копия исходного значения была уже создана в момент передачи аргумента в функцию Clone.
	// Превратим исходное значение в адресуемое reflect значение
	valAtPtr := reflect.ValueOf(&src).Elem()
	// рекурсивно проходимся по valAtPtr для глубокого копирования его частей, если это необходимо
	err := cloneNested(cloneCtx, valAtPtr)

	return src, err
}

func MustClone[T any](src T, opts ...cloneFuncOptions) T {
	v, err := Clone(src, opts...)

	if err != nil {
		panic(fmt.Sprintf("MustClone: %v", err))
	}

	return v
}

func cloneNested(cloneCtx *cloneContext, v reflect.Value) error {
	kind := v.Kind()

	// проверяем, следует ли копировать значение, т.е. не имеет ли оно ни базового типа, ни нулевого
	if ReflectIsBasicKind(kind) || v.IsZero() {
		return nil
	}

	//nolint:exhaustive // ignore
	switch kind {
	case reflect.Struct:
		// для структур итерируемся по ее полям
		for i := 0; i < v.NumField(); i++ {
			wField := v.Field(i)

			// если поле экспортируемое
			if wField.CanSet() {
				// рекурсивно клонируем это
				if err := cloneNested(cloneCtx, wField); err != nil {
					return err
				}

				continue
			}

			// если поле не экспортируемое, то смотрим какие настройки клонирования были переданы
			switch cloneCtx.unexportedStrategy {
			// ничего не делаем (поверхностная копия)
			case shallowCopyUnexportedStrategy:
				continue

			// Принудительное глубокое копирование
			case forceDeepCopyUnexportedStrategy:
				newAt := reflect.NewAt(wField.Type(), unsafe.Pointer(wField.UnsafeAddr()))
				if err := cloneNested(cloneCtx, newAt.Elem()); err != nil {
					return err
				}

			// Принудительно устанавливаем zero value
			case forceZeroUnexported:
				typ := wField.Type()
				newAt := reflect.NewAt(typ, unsafe.Pointer(wField.UnsafeAddr()))
				newAt.Elem().Set(reflect.Zero(typ))
			}
		}

	case reflect.Array:
		// для массивов проходимся по каждому значению
		elem := v.Type().Elem()
		res := reflect.New(reflect.ArrayOf(v.Len(), elem)).Elem()
		// копируем значения из исходника
		reflect.Copy(res, v)

		// если элемент является базовым типом, то просто добавляем его
		if ReflectIsBasicKind(elem.Kind()) {
			v.Set(res)
			return nil
		}

		// иначе, рекурсивно клонируем каждый элемент
		for i := 0; i < res.Len(); i++ {
			if err := cloneNested(cloneCtx, res.Index(i)); err != nil {
				return err
			}
		}

		// заменяем исходное значение копией
		v.Set(res)

	case reflect.Slice:
		// для слайсов аллоцируем память под элементы
		typ := v.Type()
		res := reflect.MakeSlice(typ, v.Len(), v.Cap())
		// and copy values from source at once
		reflect.Copy(res, v)

		// если элемент имеет базовый тип, то просто возвращаем
		if ReflectIsBasicKind(typ.Elem().Kind()) {
			v.Set(res)
			return nil
		}

		// иначе, рекурсивно клонируем элементы
		for i := 0; i < res.Len(); i++ {
			if err := cloneNested(cloneCtx, res.Index(i)); err != nil {
				return err
			}
		}

		// заменяем исходник копией
		v.Set(res)
	case reflect.Map:
		// для мап аллоцируем память
		typ := v.Type()
		res := reflect.MakeMapWithSize(typ, v.Len())

		// создаем новые значения для итерации по мапе
		iter := v.MapRange()
		newK := reflect.New(typ.Key()).Elem()
		newV := reflect.New(typ.Elem()).Elem()

		// проверяем, если ключ и значение мапы имеют базовй тип
		keyIsBasic := ReflectIsBasicKind(typ.Key().Kind())
		valueIsBasic := ReflectIsBasicKind(typ.Elem().Kind())

		for iter.Next() {
			k := iter.Key()

			// если ключ должен быть скопирован, копируем рекурсивно
			if !keyIsBasic && !k.IsZero() {
				newK.Set(k)

				if err := cloneNested(cloneCtx, newK); err != nil {
					return err
				}

				k = newK
			}

			v := iter.Value()

			// если значение должно быть скопировано, копируем рекурсивно
			if !valueIsBasic && !k.IsZero() {
				newV.Set(v)

				if err := cloneNested(cloneCtx, newV); err != nil {
					return err
				}

				v = newV
			}

			// помещаем ключ и значение в копию мапы
			res.SetMapIndex(k, v)
		}

		// заменяем исходник копией
		v.Set(res)

	case reflect.Pointer:
		// проверяем, готов ли указатель
		ptr := v.UnsafePointer()

		if newV, ok := cloneCtx.resolvePointer(ptr); ok {
			// если уже была создана копия, то копируем предыдущую копию
			v.Set(newV)
			return nil
		}

		// если указатель не был копирован, создаем новое значение
		newV := reflect.New(v.Elem().Type())

		// и помещаем в контекст
		cloneCtx.setPointer(ptr, newV)

		// помещаем в источник значение нового указателя и рекурсивно копируем его
		newV.Elem().Set(v.Elem())

		if err := cloneNested(cloneCtx, newV.Elem()); err != nil {
			return err
		}

		// заменяем исходник копией
		v.Set(newV)

	case reflect.Interface:
		// если нужно скопировать базовое значение интерфейса
		el := v.Elem()

		if ReflectIsBasicKind(el.Kind()) || el.IsZero() {
			return nil
		}

		// копируем рекурсивно
		newV := reflect.New(v.Elem().Type()).Elem()
		newV.Set(v.Elem())

		if err := cloneNested(cloneCtx, newV); err != nil {
			return err
		}

		v.Set(newV)

	default:
		// если тип не поддерживается - возвращаем ошибку
		if cloneCtx.errOnUnsupported {
			return fmt.Errorf("unsupported type: %s", v.Type().Name())
		}
	}

	return nil
}
