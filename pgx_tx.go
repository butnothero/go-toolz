package gt

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
)

var (
	ErrPgxTxNotFound = errors.New("pgx transaction not found")
)

func EndPgxTx(ctx context.Context, ctxTxKey CtxKeyPgxTx, err error) error {
	tx, ok := ctx.Value(ctxTxKey).(pgx.Tx)

	if !ok {
		return ErrPgxTxNotFound
	}

	if tx == nil {
		return ErrPgxTxNotFound
	}

	if err == nil {
		if err := tx.Commit(ctx); err != nil {
			return err
		}
	} else {
		if err := tx.Rollback(ctx); err != nil {
			return err
		}
	}

	return nil
}

func BeginPgxTx(ctx context.Context, ctxTxKey CtxKeyPgxTx, pool PgxPool, opts ...pgx.TxOptions) (context.Context, error) {
	txOpts := OptionalArg(opts)

	tx, err := pool.BeginTx(ctx, txOpts)

	if err != nil {
		return ctx, err
	}

	return context.WithValue(ctx, ctxTxKey, tx), nil
}
