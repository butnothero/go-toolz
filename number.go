package gt

import (
	"math"
	"math/big"
)

var (
	MaxBigInt    = big.NewInt(math.MaxInt)
	MaxBigInt16  = big.NewInt(math.MaxInt16)
	MaxBigInt32  = big.NewInt(math.MaxInt32)
	MaxBigInt64  = big.NewInt(math.MaxInt64)
	MaxBigUint16 = big.NewInt(math.MaxUint16)
	MaxBigUint32 = big.NewInt(math.MaxUint32)
	MaxBigUint64 = new(big.Int).SetUint64(math.MaxUint64)
)

const (
	Float32ofMaxInt32 = float32(math.MaxInt32)
	Float64ofMaxInt64 = float64(math.MaxInt64)
)
