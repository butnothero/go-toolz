package gt

import "time"

func Duration(years, months, days int) time.Duration {
	now := time.Now()
	return now.AddDate(years, months, days).Sub(now)
}
