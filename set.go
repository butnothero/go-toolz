package gt

// Set Множество с уникальными значениями.
type Set[T comparable] map[T]struct{}

// NewSet создать новый Set.
func NewSet[T comparable]() Set[T] {
	return make(Set[T])
}

// Add Добавить значение.
func (s Set[T]) Add(v T) {
	s[v] = struct{}{}
}

// Delete Удалить значение.
func (s Set[T]) Delete(v T) {
	delete(s, v)
}

// Has Имеется ли значение в Set.
func (s Set[T]) Has(v T) bool {
	_, ok := s[v]
	return ok
}

// Len Получить количество значений в Set.
func (s Set[T]) Len() int {
	return len(s)
}

// Clear Очистить Set.
func (s Set[T]) Clear() {
	for k := range s {
		delete(s, k)
	}
}

// ForEach Итерация по всем элементам Set.
func (s Set[T]) ForEach(f func(T)) {
	for v := range s {
		f(v)
	}
}
